﻿using System;
using System.Threading.Tasks;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;
namespace MqttDemo
{
    class Publisher
    {
        static async Task Main(string[] args)
        {
            var mqttFactory = new MqttFactory();
            var client =  mqttFactory.CreateMqttClient();
            var opts = new MqttClientOptionsBuilder()
                            .WithClientId("top_pub_1")
                            .WithCredentials("demo","demo@123")
                            .WithTcpServer("hfcb9b11.ap-southeast-1.emqx.cloud",15072)
                            .WithCleanSession()
                            .Build();
            client.UseConnectedHandler(e=>{ 
                Console.WriteLine("connect successfully");
            });

            client.UseDisconnectedHandler(e=>{ 
                Console.WriteLine("disconnect successfully");
            });
                    
            await client.ConnectAsync(opts);
            Console.WriteLine("typing for send message");
            var text = Console.ReadLine();
            while(text!="exit")
            {
                await PubMessageAsync(client,text);
                text =Console.ReadLine();
            }
            
            await client.DisconnectAsync();
        }

        private static async Task PubMessageAsync(IMqttClient client,string text)
        {
            string messagePayload = $"{text}";
            var mssage = new MqttApplicationMessageBuilder()
                .WithTopic("chat/send/text")
                .WithPayload(messagePayload)
                .WithAtLeastOnceQoS()
                .Build();
            if(client.IsConnected)
            {
                await client.PublishAsync(mssage);
            }
        }
    }
}
