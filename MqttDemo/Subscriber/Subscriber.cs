﻿using System;
using System.Text;
using System.Threading.Tasks;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Options;

namespace Subscriber
{
    class Subscriber
    {
        static async Task Main(string[] args)
        {
           var mqttFactory = new MqttFactory();
            var client =  mqttFactory.CreateMqttClient();
            var opts = new MqttClientOptionsBuilder()
                            .WithCredentials("demo","demo@123")
                            .WithTcpServer("hfcb9b11.ap-southeast-1.emqx.cloud",15072)
                            .WithCleanSession()
                            .Build();
            client.UseConnectedHandler(async e=>{ 
                Console.WriteLine("connect successfully");
                var topicFilter = new MqttTopicFilterBuilder()
                                .WithTopic("chat/send/text")
                                .Build();
                await client.SubscribeAsync(topicFilter);
            });

            client.UseDisconnectedHandler(e=>{ 
                Console.WriteLine("disconnect successfully");
            });
            client.UseApplicationMessageReceivedHandler(e=>{ 
                Console.WriteLine($"Received message - {Encoding.UTF8.GetString(e.ApplicationMessage.Payload)}");
            });   
            await client.ConnectAsync(opts);
            Console.ReadLine();
            await client.DisconnectAsync();
        }
    }
}
